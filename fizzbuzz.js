function FizzBuzz(maxValue) {
    let x = 0
    let v = "";
    while (x <= maxValue) {
        if ((x % 2 === 0) && (x % 3 === 0)) {
            v = v +"FizzBuzz, "
        }
        else if (x % 2 === 0) {
            v = v + "Fizz, "
        }
        else if (x % 3 === 0) {
            v = v + "Buzz, "
        }
        else {
            v = v + x + ", "
        }
        x++
    }
    console.log(v)
}

FizzBuzz(20);
